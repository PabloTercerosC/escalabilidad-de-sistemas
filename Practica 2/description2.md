# Practice 2: Relational Database Scaling.
In a relational database, that already has data, apply one of the following options:

- Replication (3 nodes) (I will implement replication)
- Clustering (2 nodes)
- Sharding (2 nodes)

Create a Merge Request in Gitlab and assign to: gustavo.rodriguez@fundacion-jala.org
 
The MR should contain sufficient description to perform the practice (including db schemas and queries). Description of the steps followed and how it was done, include some screenshots.
 
Deadline: May 29th at noon

## Steps
Follow the steps in the README:

1. Start the docker compose:
![alt text](<Screenshot from 2024-05-24 13-09-10.png>)

2. Execute master bash and write my.cnf
![alt text](<Screenshot from 2024-05-24 13-09-23.png>)

3. Execute repl1 bash and write my.cnf
![alt text](<Screenshot from 2024-05-24 13-09-29.png>)

4. Execute repl2 bash and write my.cnf
![alt text](<Screenshot from 2024-05-24 13-09-35.png>)

5. Restart, stop and start docker
![alt text](<Screenshot from 2024-05-24 13-10-24.png>)

6. Show master status
![alt text](<Screenshot from 2024-05-24 13-11-38.png>)

7. Start repl1 slave
![alt text](<Screenshot from 2024-05-24 13-13-31.png>)

8. Start repl2 slave
![alt text](<Screenshot from 2024-05-24 13-14-53.png>)

9. Create a table in master and insert some data
![alt text](<Screenshot from 2024-05-24 13-15-36.png>)

10. Check the data in repl1
![alt text](<Screenshot from 2024-05-24 13-16-07.png>)

11. Check the data in repl2
![alt text](<Screenshot from 2024-05-24 13-16-10.png>)