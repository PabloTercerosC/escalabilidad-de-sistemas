```bash
docker compose up -d
docker compose logs
```

----------------master------------------
```bash
docker exec -it mysql-master bash
apt update
apt install vim -y
vim /etc/mysql/my.cnf
```

```text
[mysqld]
skip-name-resolve
default_authentication_plugin = mysql_native_password
log-bin=mysql-bin
server-id=1
binlog_format = ROW
```
----------------repl1------------------

```bash
docker exec -it mysql-rep1 bash
apt update
apt install vim -y
vim /etc/mysql/my.cnf
```

```text
[mysqld]
skip-name-resolve
default_authentication_plugin = mysql_native_password
server-id=2
log-bin=mysql-replica-bin
relay-log=relay-log
```

----------------repl2------------------

```bash
docker exec -it mysql-rep2 bash
apt update
apt install vim -y
vim /etc/mysql/my.cnf
```

```text
[mysqld]
skip-name-resolve
default_authentication_plugin = mysql_native_password
server-id=3
log-bin=mysql-replica2-bin
relay-log=relay-log
```

----------------------------------------

```bash
docker compose restart
docker compose stop
docker compose start
```

----------------master------------------

```bash
docker exec -it mysql-master mysql -u root -p
```

```sql
CREATE USER 'repl'@'%' IDENTIFIED BY 'pw';

GRANT REPLICATION SLAVE ON *.* TO 'repl'@'%';

FLUSH PRIVILEGES;

SHOW MASTER STATUS;
```

----------------repl1------------------

```bash
docker exec -it mysql-rep1 mysql -u root -p
```

```sql
CHANGE MASTER TO MASTER_HOST='mysql-master', MASTER_USER='repl', MASTER_PASSWORD='pw', MASTER_LOG_FILE='mysql-bin.000002', MASTER_LOG_POS=827;

START SLAVE;

SHOW SLAVE STATUS\G
```

----------------repl2------------------

```bash
docker exec -it mysql-rep2 mysql -u root -p
```

```sql
CHANGE MASTER TO MASTER_HOST='mysql-master', MASTER_USER='repl', MASTER_PASSWORD='pw', MASTER_LOG_FILE='mysql-bin.000002', MASTER_LOG_POS=827;

START SLAVE;

SHOW SLAVE STATUS\G
```

----------------------------------------

```sql
CREATE TABLE Personas(
  Nombre VARCHAR(128),
  Apellido VARCHAR(128)
);

INSERT INTO Personas (Nombre, Apellido) VALUES
 ('1', 'Uno'),
 ('2', 'Dos'),
 ('3', 'Tres');
 
SELECT * FROM Personas;
```

----------------------------------------

```bash
docker compose logs
docker compose down
docker volume prune
```