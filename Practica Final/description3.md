# Práctica 3: Explorando funcionalidades de NoSql.

## Instrucciones
En una base de datos NoSql (Mongodb), aplicar al requerimiento donde el volumen de datos no sea una básico y que permita experimentar los diferentes tiempos de respuesta que se tienen en diferentes escenarios con diferentes volúmenes de datos. Revisar estos tiempos de respuesta pueden mejorarse (sin recurrir a escalabilidad)

(Opcionalmente) Comparar con una versión equivalente Sql.


Crear un Merge Request en Gitlab y asignar a: gustavo.rodriguez@fundacion-jala.org
 

El MR debe contener la descripción suficiente para realizar la práctica (incluido los esquemas de db y queries, código). Descripción de los pasos seguidos y como se realizó, incluir algunas capturas.
 
Fecha límite: 5 de Junio a medio día


## Contexto

_"Una startup lanza una red social para entusiastas de hobbies poco comunes con una configuración inicial limitada de MongoDB, adecuada para un crecimiento moderado. Sin embargo, tras ganar popularidad rápidamente debido a eventos virales y alianzas con influencers, la plataforma experimenta un aumento masivo en el registro de usuarios y actividad. La base de datos, inicialmente limitada a un solo CPU y 600 MB de memoria, enfrenta problemas de rendimiento y escalabilidad._

_El equipo se ve forzado a reevaluar y escalar la infraestructura urgentemente, considerando la migración a soluciones más robustas y distribuidas para manejar la creciente carga de datos y asegurar la estabilidad del sistema. Esta situación subraya la importancia de prepararse para el escalado rápido frente a crecimientos inesperados en el sector tecnológico"._

## Requisitos:
- data.zip
- posts-mongo.zip
- docker-compose
- REST CLIENT Extension

## Set Up

1. Renombrar el archivo: 
```
compose 2.yaml -> docker-compose.yml
```
2. Levantar la DB:
```
sudo docker compose up -d
```
3. Ejecutar la DB:
```
sudo docker exec -it mo1 mongosh -u admin -p pw --authenticationDatabase admin
```
4. Descomprimir ambos ficheros. "posts-mongo.zip" incluye modelos y rutas mongoose y express respectivamente, mientras data.zip contiene un volúmen considerable de datos.

## Pruebas

Vamos a probar las peticiones http de la carpeta http_tests en VS Code con la extensión de REST CLIENT (https://marketplace.visualstudio.com/items?itemName=humao.rest-client) y mostrar los tiempos de respuesta para cada petición (recordemos que en posts por ejemplo tenemos 39993086 documentos registrados):

**Comment:**
1. Get all Comments: 18ms
2. Register Comment: 16ms

**Post:**
1. Get all Post: 23950ms
2. Register Post: 21ms
3. Update Post: 16ms
4. Delete Post: 12ms

**Like:**
1. Sends like: 14ms

**User:**
1. Get all users: 617ms
2. Register user: 23ms

## Resultados
En base a estos resultados, podemos opinar lo siguiente de los tiempos de respuesta:

**Comment:** Estos tiempos son excelentes y proporcionan una respuesta casi instantánea, lo cual es ideal para la interactividad en tiempo real que se espera en funciones sociales como los comentarios.

**Post:** El tiempo de respuesta para obtener todos los posts es extremadamente alto, casi 24 segundos, lo cual es inaceptable en un entorno de producción y deterioraría significativamente la experiencia del usuario. Las operaciones de registro, actualización y eliminación de posts son rápidas y eficientes.

**Like:** Este tiempo es muy bueno, permitiendo a los usuarios interactuar con el contenido de manera rápida y eficiente, lo que es crucial para la participación en la plataforma.

**User:** Mientras que el registro de usuario es rápido, obtener todos los usuarios toma más de medio segundo, lo que podría ser mejorable dependiendo del contexto de uso. Si es una operación que raramente necesita ser realizada por un usuario común (por ejemplo, solo por administradores), podría ser aceptable; de lo contrario, sería beneficioso optimizar esta operación.

# Conclusiones
La evaluación de los tiempos de respuesta en las operaciones CRUD de la red social indica necesidades críticas de optimización, especialmente en la obtención de posts, que muestra un tiempo de respuesta significativamente alto.

# Recomendaciones:

**Optimización de Consultas e Índices:** Revisar y optimizar las consultas de la base de datos, especialmente para "Get all Posts". Asegurar que los campos utilizados en filtros de consultas estén indexados adecuadamente.

**Implementación de Paginación:** Aplicar paginación en la carga de posts y usuarios para reducir la carga de datos por solicitud y mejorar la eficiencia de las respuestas.

**Caché de Datos:** Utilizar soluciones de caché, como Redis, para almacenar resultados frecuentes y datos estáticos, disminuyendo así las cargas directas a la base de datos.

**Revisión del Modelo de Datos:** Analizar y ajustar el esquema de datos para optimizar las operaciones, considerando la normalización o desnormalización según las necesidades de consulta.

**Configuración del Servidor de Base de Datos:** Optimizar configuraciones de MongoDB para mejorar la utilización de los recursos disponibles dentro de los límites actuales.

**Compresión de Datos:** Implementar técnicas de compresión de datos para agilizar la transferencia y procesamiento de grandes volúmenes de información.