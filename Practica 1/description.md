# Practica 1: Database Partitioning
Instruction: "In a relational database that has some tables with data, apply Horizontal Partitioning for range, list, hash and key".

## Steps
1. Create a Docker Container for MySQL: https://hub.docker.com/_/mysql
2. Create a MySQL Container:
```
$ sudo docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:latest
```
3. This will also start the container. Start a bash session of the container:
```
$ sudo docker exec -it some-mysql bash
```
4. Log In to the container:
```
$ mysql -u root -p
```
5. Enter the password of the ROOT user ("my-secret-pw" in this case)
6. Create a database:
```
CREATE DATABASE db;
```
7. Change to the DB context:
```
USE db
```
8. Execute the queries from the "Partition Queries" section.


## Partition Queries

RANGE PARTITIONING:
```
CREATE TABLE employees1 (
    id INT NOT NULL,
    fname VARCHAR(30),
    lname VARCHAR(30),
    hired DATE NOT NULL DEFAULT '1970-01-01',
    separated DATE NOT NULL DEFAULT '9999-12-31',
    job_code INT NOT NULL,
    store_id INT NOT NULL
)
PARTITION BY RANGE (store_id) (
    PARTITION p0 VALUES LESS THAN (6),
    PARTITION p1 VALUES LESS THAN (11),
    PARTITION p2 VALUES LESS THAN (16),
    PARTITION p3 VALUES LESS THAN (21)
);
```

LIST PARTITIONING:
```
CREATE TABLE employees2 (
    id INT NOT NULL,
    fname VARCHAR(30),
    lname VARCHAR(30),
    hired DATE NOT NULL DEFAULT '1970-01-01',
    separated DATE NOT NULL DEFAULT '9999-12-31',
    job_code INT,
    store_id INT
)
PARTITION BY LIST(store_id) (
    PARTITION pNorth VALUES IN (3,5,6,9,17),
    PARTITION pEast VALUES IN (1,2,10,11,19,20),
    PARTITION pWest VALUES IN (4,12,13,14,18),
    PARTITION pCentral VALUES IN (7,8,15,16)
);
```

HASH PARTITIONING:
```
CREATE TABLE employees3 (
    id INT NOT NULL,
    fname VARCHAR(30),
    lname VARCHAR(30),
    hired DATE NOT NULL DEFAULT '1970-01-01',
    separated DATE NOT NULL DEFAULT '9999-12-31',
    job_code INT,
    store_id INT
)
PARTITION BY HASH( YEAR(hired) )
PARTITIONS 4;
```

KEY PARTITIONING: 
```
CREATE TABLE k1 (
    id INT NOT NULL PRIMARY KEY,
    name VARCHAR(20)
)
PARTITION BY KEY()
PARTITIONS 2;
```

## Links
- What is data partitioning?: https://www.cockroachlabs.com/blog/what-is-data-partitioning-and-how-to-do-it-right/
- Data partitioning guidance: https://learn.microsoft.com/en-us/azure/architecture/best-practices/data-partitioning
- MySQL Partitioning Docs: https://dev.mysql.com/doc/refman/8.0/en/partitioning-types.html
- Range Example: https://dev.mysql.com/doc/refman/8.0/en/partitioning-range.html
- List Example: https://dev.mysql.com/doc/refman/8.0/en/partitioning-list.html
- Hash Example: https://dev.mysql.com/doc/refman/8.0/en/partitioning-hash.html
- Key Example: https://dev.mysql.com/doc/refman/8.0/en/partitioning-key.html

## Gallery
- Docker Hub: MySQL Website:
![alt text](image.png)
- MySQL docs Website:
![alt text](image-1.png)
- MySQL Docker Status is UP:  
![alt text](image-2.png)
- Welcome to MySQL root user!:  
![alt text](image-3.png)
- The partitioned tables were added successfully:
![alt text](image-4.png)
- FINALLY, we can insert data into a table, and check how the partition divides the table into fragments:
![alt text](image-5.png)